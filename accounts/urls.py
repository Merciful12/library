from django.conf.urls import url
from . import views
from django.contrib.auth import views as auth_views


app_name = 'accounts'
urlpatterns = [
    url(r'^register/$', views.RegisterFormView.as_view(),{'next_page' :'library:index'},name='register'),
    url(r'^login/$',auth_views.login,{'redirect_authenticated_user' :'library:index'},name='login',),
    url(r'^logout/$',auth_views.logout,{'next_page' :'library:index'},name='logout'),
]