from django.db import models
from django.core.urlresolvers import reverse


class Author(models.Model):
    name = models.CharField(max_length=100)

    def get_absolute_url(self):
        return reverse('library:author',
                       args=[str(self.id)])

    def __str__(self):
        return self.name


class Book(models.Model):
    title = models.CharField(max_length=255)
    author = models.ForeignKey(Author,
                               on_delete=models.SET_NULL,
                               null=True,
                               blank=True)

    def get_absolute_url(self):
        return reverse('library:book',
                       args=[str(self.id)])

    def __str__(self):
        return self.title