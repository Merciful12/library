from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
from django.views.decorators.http import require_GET
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from .models import Book,Author


@require_GET
def Home(request):
    template = 'index.html'
    cnt_authors = Author.objects.all().count()
    cnt_books = Book.objects.all().count()
    context = {
        'cnt_books': cnt_books,
        'cnt_authors': cnt_authors
    }
    return render (request,template,context)


#book
class BookListView(generic.ListView):
    model = Book
    paginate_by = 10


class BookDetailView(generic.DetailView):
    model = Book
    template_name = 'library/book.html'


class BookCreate(LoginRequiredMixin, CreateView):
    model = Book
    fields = '__all__'
    template_name = 'library/book_new.html'


class BookUpdate(LoginRequiredMixin, UpdateView):
    model = Book
    fields = '__all__'
    template_name = 'library/book_edit.html'


class BookDelete(LoginRequiredMixin, DeleteView):
    model = Book
    success_url = reverse_lazy('library:books')
    template_name = 'library/book_del.html'


#author
class AuthorListView(generic.ListView):
    model = Author
    paginate_by = 10


class AuthorDetailView(generic.DetailView):
    model = Author
    template_name = 'library/author.html'


class AuthorCreate(LoginRequiredMixin,CreateView):
    model = Author
    fields = '__all__'
    template_name = 'library/author_new.html'


class AuthorUpdate(LoginRequiredMixin, UpdateView):
    model = Author
    fields = '__all__'
    template_name = 'library/author_edit.html'



class AuthorDelete(LoginRequiredMixin, DeleteView):
    model = Author
    success_url = reverse_lazy('library:authors')
    template_name = 'library/author_del.html'
