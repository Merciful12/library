from django.conf.urls import url

from . import views

app_name = 'library'
urlpatterns = [
    url(r'^$', views.Home, name='index'),
]

#Book
urlpatterns += [
    url(r'^books/$',views.BookListView.as_view(),name='books'),
    url(r'^book/(?P<pk>[0-9]+)$', views.BookDetailView.as_view(), name='book'),
    url(r'^book/add/$', views.BookCreate.as_view(), name='book_add'),
    url(r'^book/(?P<pk>[0-9]+)/edit/$', views.BookUpdate.as_view(), name='book_edit'),
    url(r'^book/(?P<pk>[0-9]+)/delete/$', views.BookDelete.as_view(), name='book_del'),
]

#Author
urlpatterns += [
    url(r'^authors/$',views.AuthorListView.as_view(),name='authors'),
    url(r'^author/(?P<pk>[0-9]+)$', views.AuthorDetailView.as_view(), name='author'),
    url(r'^author/add/$', views.AuthorCreate.as_view(), name='author_add'),
    url(r'^author/(?P<pk>[0-9]+)/edit/$',views.AuthorUpdate.as_view(), name='author_edit'),
    url(r'^author/(?P<pk>[0-9]+)/delete/$', views.AuthorDelete.as_view(), name='author_del'),
]