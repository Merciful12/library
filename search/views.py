from django.db.models import Q
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from library.models import Book


def search(request):
    template = 'search/search.html'
    query = request.GET.get('q')
    if query:
        books = Book.objects.filter(Q(title__icontains=query) |
                                    Q(author__name__icontains=query))
        return render(request, template, {'books': books})
    return render(request, template, {'books':None})